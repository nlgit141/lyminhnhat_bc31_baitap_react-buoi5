import { ADD_TO_CART, REMOVE_SHOE_CART, STEP_SHOE } from '../constant/constant';
import { ShoeArr } from '../Datashoe';

export let initalState = {
    shoeArr: ShoeArr,
    cartShoe: []
};

export let shoeShopReducer = (state = initalState, { type, payload }) => {
    switch (type) {


        // add to cart
        case ADD_TO_CART: {
            let index = state.cartShoe.findIndex((item) => {
                return item.id == payload.id;
            });
            let cloneCartShoe = [...state.cartShoe];
            if (index == -1) {
                let newSp = { ...payload, soLuong: 1, total: 0 };
                newSp.total = newSp.price * newSp.soLuong;
                cloneCartShoe.push(newSp);
            } else {
                cloneCartShoe[index].soLuong++;
                cloneCartShoe[index].total = cloneCartShoe[index].price * cloneCartShoe[index].soLuong;
            }
            state.cartShoe = cloneCartShoe;
            return { ...state };
        }


        // remove shoe
        case REMOVE_SHOE_CART: {
            let index = state.cartShoe.findIndex((item) => {
                return item.id == payload;
            });
            let cloneCartShoe = [...state.cartShoe];
            if (index !== -1) {
                cloneCartShoe.splice(index, 1);
                state.cartShoe = cloneCartShoe;
            }
            return { ...state };
        }


        // plus quanitity
        case STEP_SHOE: {
            let index = state.cartShoe.findIndex((item) => {
                return item.id == payload.SpId;
            });
            let cloneCartShoe = [...state.cartShoe];
            if (index !== -1) {
                cloneCartShoe[index].soLuong = cloneCartShoe[index].soLuong + payload.step;
                cloneCartShoe[index].total = cloneCartShoe[index].total + cloneCartShoe[index].price * payload.step;
            }
            if (cloneCartShoe[index].soLuong == 0) {
                cloneCartShoe.splice(index, 1);
            }
            state.cartShoe = cloneCartShoe;
            return { ...state };
        }


        default:
            return state;
    }
};