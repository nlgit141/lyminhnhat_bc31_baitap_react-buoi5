import { combineReducers } from "redux";
import { shoeShopReducer } from './shoeShopReducer';

export let rootReducerShoeShop = combineReducers({
    shoeShopReducer
});