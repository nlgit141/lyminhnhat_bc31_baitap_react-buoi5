import React, { Component } from 'react';
import { connect } from 'react-redux';
import ItemShoe from './ItemShoe';

class ListShoe extends Component {
    renderItemShoe = () => {
        return this.props.itemShoe.map((item) => {
            return <ItemShoe dataShoe={item} key={item.id} />;
        });
    };

    render() {
        return (
            <div >
                <div className='row'>
                    {this.renderItemShoe()}
                </div>
            </div>
        );
    }
};

let mapStateToProps = (state) => {
    return {
        itemShoe: state.shoeShopReducer.shoeArr
    };
};

export default connect(mapStateToProps)(ListShoe);