import React, { Component } from 'react';
import { connect } from 'react-redux';
import { REMOVE_SHOE_CART, STEP_SHOE } from './constant/constant';
import shortId from 'shortid';

class TableShoe extends Component {
    renderContent = () => {
        return this.props.cartShoe.map((item) => {
            let randomKey = shortId.generate();
            return (
                <tr key={randomKey} >
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>
                        <img src={item.image} alt="" style={{ width: "40px" }} />
                    </td>
                    <td>${item.price}</td>
                    <td>
                        <button onClick={() => {
                            this.props.handleStep(item.id, -1);
                        }} className='btn btn-warning'>-</button>

                        <span className='mx-2'>{item.soLuong}</span>

                        <button onClick={() => {
                            this.props.handleStep(item.id, +1);
                        }} className='btn btn-danger'>+</button>
                    </td>
                    <td>${item.total}</td>
                    <td>
                        <button onClick={() => {
                            this.props.handleRemove(item.id);
                        }} className='btn btn-dark'>Delete</button>
                    </td>
                </tr>);
        });
    };
    render() {
        return (
            <div>
                {this.props.cartShoe.length > 0 && <table className='table'>
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>NAME</td>
                            <td>PRICE</td>
                            <td>DESCRIPTION</td>
                            <td>QUANITITY</td>
                            <td>TOTAL</td>
                            <td>ACTION</td>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderContent()}
                    </tbody>
                </table>}
            </div>
        );
    }
}

let mapStateToProps = (state) => {
    return {
        cartShoe: state.shoeShopReducer.cartShoe
    };
};
let mapDisPatchToProps = (dispatch) => {
    return {
        handleRemove: (sp) => {
            dispatch({
                type: REMOVE_SHOE_CART,
                payload: sp
            });
        },
        handleStep: (SpId, step) => {
            dispatch({
                type: STEP_SHOE,
                payload: { SpId, step }
            });
        },
    };
};

export default connect(mapStateToProps, mapDisPatchToProps)(TableShoe);