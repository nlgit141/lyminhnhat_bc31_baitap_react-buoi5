import React, { Component } from 'react';
import ListShoe from './ListShoe';
import TableShoe from './TableShoe';



export default class ShoeShop extends Component {


    render() {
        return (
            <div >
                <div className='container'>
                    <h1>SHOE-SHOP</h1>
                    <TableShoe />
                    <ListShoe />
                </div>
            </div>
        );
    }
}
